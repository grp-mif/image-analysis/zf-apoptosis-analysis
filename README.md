# Zebrafish Apoptosis analysis

## Description
In this project we aim at analyzing the cell number and division rate of cells in developing zebrafish embryos.

## Assay
Zebrafish are imaged in the MuVi SPIM system from Luxendo.

## Data acquisition
Multiple samples are mounted in a FEP tube and image over time for approx XX hours with a time interval of XX min.

## Data management

Please refer to the [Data Management document](data_management.md).

## Usage of this repository

1. Download the repository using the download button at the top right:
![download_button](documentation/download_button.PNG)
1. Follow the [Workflow document](analysis_workflow.md) for a detailed description on how to run the code.
