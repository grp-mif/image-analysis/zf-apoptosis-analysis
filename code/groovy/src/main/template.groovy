import ij.IJ
import ij.ImageJ
import inra.ijpb.plugins.AnalyzeRegions
import org.apache.commons.io.FilenameUtils

// INPUT UI
//
//#@ File (label="Label mask file") inputLabelFile
//#@ Boolean (label="Run headless", default="false") headless
//#@ Boolean (label="Save results", default="true") saveResults

// IDE
//
//def inputLabelFile = new File("/Users/tischer/Documents/matt-govendir-blood-vessels-malaria/data/output/2d-ch0-ch1mem-ch2nuc_cp_masks.tif")
def inputLabelFile = new File("/home/anaacayuela/Ana_pruebas_imageJ/javi_conesa/20210309_CCT_632_bare_grid_stack_Processed.tif")
def headless = true
def saveResults = true
new ImageJ().setVisible(true)

// open and extract metadata
//
def image = IJ.openImage(inputLabelFile.toString());
image.show();
def outputDir = inputLabelFile.getParent();
def datasetId = FilenameUtils.removeExtension(inputLabelFile.getName());

// process
//
def analyzeRegions = new AnalyzeRegions(); // https://github.com/ijpb/MorphoLibJ/issues/60
def resultsTable = analyzeRegions.process(image);

// show
//
if (!headless)
{
    image.show();
    resultsTable.show(datasetId);
}

// save
//
if (saveResults) {
    def tablePath = new File(outputDir, datasetId + ".csv").toString()
    IJ.log("Saving table: " + tablePath )
    resultsTable.save(tablePath)
}
