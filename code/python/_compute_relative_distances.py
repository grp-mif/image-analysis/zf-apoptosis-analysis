import numpy as np
import tqdm

def compute_relative_distances(points_df, embryo_df):
    
    rel_dist_center = [0 for i in points_df.label]
    rel_dist_surface = [0 for i in points_df.label]

    abs_dist_center = points_df.distance_from_embryo_center.values
    abs_dist_surface = points_df.distance_from_embryo_surface.values

    tps_list = points_df.tp.values
    
    embryo_radius = np.array([ embryo_df[embryo_df.tp==tp].embryo_radius.values[0] for tp in tqdm.tqdm(tps_list, total=len(tps_list)) ])

    rel_dist_center = abs_dist_center/embryo_radius
    rel_dist_surface = abs_dist_surface/embryo_radius

    points_df["rel_distance_center"] = rel_dist_center
    points_df["rel_distance_surface"] = rel_dist_surface

    return points_df