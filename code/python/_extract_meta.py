import os, glob
import h5py

def extract_meta(path):
    sample_meta = {}

    infos = path.split(os.sep)[-3]
    infos = infos.split("_")
    sample_meta["date"] = infos[0]

    infos = path.split(os.sep)[-1]
    infos = infos.split("_")
    sample_meta["sample"] = infos[-3]
    sample_meta["condition"] = infos[-2]
    n_h5 = (len(glob.glob(os.path.join(path,"*.h5")))-1)
    n_h5_ch0 = len(glob.glob(os.path.join(path,"*ch-0*.h5")))
    sample_meta["n_ch"] = n_h5//n_h5_ch0
    sample_meta["n_tp"] = n_h5//sample_meta["n_ch"]

    sample_meta["ch-%d_index"%0] = 0
    sample_meta["ch-%d_name"%0] = "nuclear"
    sample_meta["ch-%d_wavelength"%0] = 488

    sample_meta["ch-%d_index"%1] = 1
    sample_meta["ch-%d_name"%1] = "other"
    sample_meta["ch-%d_wavelength"%1] = 561

    meta_file = glob.glob(os.path.join(path, "*.xml"))[0]

    import xml.dom.minidom
    xml_doc = xml.dom.minidom.parse(meta_file)
    unit = xml_doc.getElementsByTagName("SpimData")[0].getElementsByTagName("ViewSetups")[0].getElementsByTagName("ViewSetup")[0].getElementsByTagName("voxelSize")[0].getElementsByTagName("unit")[0].childNodes[0].data
    size = xml_doc.getElementsByTagName("SpimData")[0].getElementsByTagName("ViewSetups")[0].getElementsByTagName("ViewSetup")[0].getElementsByTagName("voxelSize")[0].getElementsByTagName('size')[0].childNodes[0].data

    sample_meta["scale_1_1_1_unit"] = unit
    sample_meta["scale_1_1_1_z"] = [float(s) for s in size.split(" ")][2]
    sample_meta["scale_1_1_1_y"] = [float(s) for s in size.split(" ")][1]
    sample_meta["scale_1_1_1_x"] = [float(s) for s in size.split(" ")][0]

    return sample_meta

def test_extract_meta():
    path = os.path.join("//ebisuya.embl.es","ebisuya","input-tray","Cell_count",
                        "MC20230609_MM2_750_DAPI-405_SOX2-488_TBX6-594_BRA-647",
                        "2023-09-06_193223")
    sample_meta = extract_meta(path)
    print(sample_meta)
