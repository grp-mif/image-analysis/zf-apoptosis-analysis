import os
import glob
import argparse
from _analyze_cells import analyze_cells

'''
To run this script:

>>> python run_cell_analysis.py --share /mif-users --radius 1,1,1 --min_distance 1
'''

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

parser = argparse.ArgumentParser()
parser.add_argument("--share", 
                    type=str, default="/mif-users", 
                    help="Server share. /mif-users (Linux) or //mif-users.embl.es/mif-users (Windows)")
parser.add_argument("--master_folder",
                    type=str, default="Users/Laura_Bianchi")
parser.add_argument("--exp_folder",
                    type=str, default="MuVi_processed")
parser.add_argument("--use_gpu",
                    type=str, default="False")

args = parser.parse_args()

share = args.share
master_folder = args.master_folder
exp_folder = args.exp_folder
use_gpu = args.use_gpu=="True"

#--------------------------------------------------------

paths = glob.glob(os.path.join(share, master_folder, exp_folder, "*"))
paths.sort()
# paths = [path for path in paths if "_BAD" not in path]
paths = [path for path in paths if "_new" in path]
print(paths)

for path in paths:
    print(10*"-",path)
    df = analyze_cells(path, use_gpu)
