import glob, os, tqdm, time
import pandas as pd
from _extract_meta import extract_meta
from _open_and_downsample_h5 import open_and_downsample_h5
from OpenIJTIFF import save_ij_tiff

def preprocess(path, share, master_folder,
                    desired_scale = 2.
                ):
    meta = extract_meta(path)
    print(meta)

    output_folder = os.path.join(share, master_folder, "MuVi_processed", "%s_%s_%s"%(meta["date"], meta["sample"], meta["condition"]))
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    meta = pd.Series(meta)
    meta.to_csv(os.path.join(output_folder, "meta.csv"))

    ### Read all images and downsample

    for tp in tqdm.tqdm(range(meta["n_tp"]), total=meta["n_tp"]):
        ### read image
        print("Starting image read...")

        start = time.time()
        for ch in range(meta["n_ch"]):
            image = open_and_downsample_h5(path, ch=ch, tp=tp, sample_meta=meta, desired_scale=desired_scale)
            _ = save_ij_tiff(
                        os.path.join(output_folder, "ch-%d_tp-%03d.tif"%(ch,tp)),
                        image,
                        ax_names = "ZYX",
                        ax_scales = [desired_scale for i in "ZYX"],
                        ax_units = ["um" for i in "ZYX"],
                )
        print("Time to read and downsample timepoint: %.2f minutes."%((time.time()-start)/60))
