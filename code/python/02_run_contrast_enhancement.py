import os
import glob
import argparse
from _enhance_contrast_folder import enhance_contrast_folder

'''
To run this script:

>>> python run_preprocessing.py --share /mif-users --radius_enhanced 1,1,1
'''

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

parser = argparse.ArgumentParser()
parser.add_argument("--share", 
                    type=str, default="/mif-users", 
                    help="Server share. /mif-users (Linux) or //mif-users.embl.es/mif-users (Windows)")
parser.add_argument("--master_folder",
                    type=str, default="Users/Laura_Bianchi")
parser.add_argument("--exp_folder",
                    type=str, default="MuVi_processed")
parser.add_argument("--use_gpu",
                    type=str, default="False")

parser.add_argument("--radius_enhanced", 
                    type=list_of_floats, default=1.,
                    help="Desired radius enhancement in um (provide one value for isotropic ZYX dimension).")
parser.add_argument("--background", 
                    type=list_of_floats, default=150.,
                    help="Constant value to subtract to the image.")

args = parser.parse_args()

share = args.share
master_folder = args.master_folder
exp_folder = args.exp_folder
use_gpu = args.use_gpu=="True"
print(use_gpu)
radius_enhanced = args.radius_enhanced
background = args.background

#--------------------------------------------------------

paths = glob.glob(os.path.join(share, master_folder, exp_folder, "*"))
paths.sort()
# paths = [path for path in paths if "2023" in path]
print(paths)

for path in paths:
    print(10*"-",path)
    enhance_contrast_folder(path, share, master_folder, radius_enhanced, background, use_gpu)