import os
import napari
import numpy as np
from OpenIJTIFF import open_ij_tiff
import pandas as pd

folder = "/mif-users/Users/Laura_Bianchi/MuVi_processed_new/2023-11-20_sample0_apoptotic"

image, name, scale, unit = open_ij_tiff(os.path.join(folder, "ch-0_tp-000.tif"))
print(image.shape)
points_df = pd.read_csv(os.path.join(folder, "cells_props.csv"))

v = napari.Viewer()

v.add_image(image)
points_df = points_df[points_df.tp==0]
points = points_df[["centroid_0","centroid_1","centroid_2"]].to_numpy()/scale

points_layer = v.add_points(points, size=30)


