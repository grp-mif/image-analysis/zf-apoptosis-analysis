import os
import glob
import argparse
from _segment_cells import segment_cells

'''
To run this script:

>>> python run_segment_cells.py --share /mif-users --radius 1,1,1 --min_distance 1
'''

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

parser = argparse.ArgumentParser()
parser.add_argument("--share", 
                    type=str, default="/mif-users", 
                    help="Server share. /mif-users (Linux) or //mif-users.embl.es/mif-users (Windows)")
parser.add_argument("--master_folder",
                    type=str, default="Users/Laura_Bianchi")
parser.add_argument("--exp_folder",
                    type=str, default="MuVi_processed")
parser.add_argument("--use_gpu",
                    type=str, default="False")

parser.add_argument("--radius_gauss", 
                    type=list_of_floats, default=1.,
                    help="Radius for gaussian blur in um.")
parser.add_argument("--min_distance", 
                    type=int, default=1,
                    help="Min distance between peaks in um.")
parser.add_argument("--threshold_peak", 
                    type=int, default=20,
                    help="Min intensity values for peaks (in percentile).")

parser.add_argument("--nucleus_radius_lims", 
                    type=list_of_floats, default=[2., 15.],
                    help="Min and max nucleus radius in um.")
parser.add_argument("--threshold_watershed", 
                    type=int, default=10,
                    help="Watershed threshold (in percentile).")

parser.add_argument("--max_distance_from_surface", 
                    type=int, default=100,
                    help="Maximum allowed disatnce from surface of fitted sphere (in um).")
parser.add_argument("--max_distance_to_10neighbors", 
                    type=int, default=50,
                    help="Maximum allowed average distance to the 10 closest neighbors (in um).")


args = parser.parse_args()

share = args.share
master_folder = args.master_folder
exp_folder = args.exp_folder
use_gpu = args.use_gpu=="True"

radius_gauss = args.radius_gauss
min_distance = args.min_distance
threshold_peak = args.threshold_peak

nucleus_radius_lims = args.nucleus_radius_lims
threshold_watershed = args.threshold_watershed

max_distance_from_surface = args.max_distance_from_surface
max_distance_to_10neighbors = args.max_distance_to_10neighbors

min_nucleus_radius = nucleus_radius_lims[0]
max_nucleus_radius = nucleus_radius_lims[1]

#--------------------------------------------------------

paths = glob.glob(os.path.join(share, master_folder, exp_folder, "*"))
paths.sort()
paths = [path for path in paths if "_BAD" not in path]
# paths = [path for path in paths if "_new" in path]
print(paths)

for path in paths:
    print(10*"-",path)
    segment_cells(path, radius_gauss, min_distance, threshold_peak, 
            min_nucleus_radius, max_nucleus_radius, threshold_watershed, 
            max_distance_from_surface, max_distance_to_10neighbors, use_gpu)
