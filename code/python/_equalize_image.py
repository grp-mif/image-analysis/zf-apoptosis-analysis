import numpy as np
from skimage.filters import gaussian

def equalize_image(image, ax_scale, radius=1.):
    sigma = 10*np.array(3*[radius])/np.array(ax_scale)
    g = gaussian(image.astype(float), sigma=sigma, truncate=1.0, preserve_range=True)
    print(np.min(g), np.max(g))
    g = (g-np.min(g))/(np.max(g)-np.min(g))
    print(np.min(g), np.max(g))
    g = g*(g>0.001)#0.02)
    print(np.min(g), np.max(g))
    g[g==0] = np.median(g[g!=0])
    print(np.min(g), np.max(g))
    g = g/np.median(g)
    print(np.min(g), np.max(g))

    print(np.min(image), np.max(image))
    image_equalized = image/g
    print(np.min(image_equalized), np.max(image_equalized))
    image_equalized = image_equalized.astype(np.uint16)
    print(np.min(image_equalized), np.max(image_equalized))

    return image_equalized
