import os, tqdm
from OpenIJTIFF import open_ij_tiff
import numpy as np
from skimage.measure import regionprops_table
import pandas as pd
from _read_meta_csv import read_meta_csv
from _fit_sphere import fit_sphere
from skimage.measure import marching_cubes, mesh_surface_area
from skimage.morphology import binary_erosion
from scipy.spatial import ConvexHull
from sklearn.neighbors import NearestNeighbors
from scipy.spatial import Delaunay, tsearch
from _compute_relative_distances import compute_relative_distances

def analyze_cells(path, use_gpu=True):
    if use_gpu:
        print("Using GPU label images.")
    else:
        print("Using CPU label images.")
    meta = read_meta_csv(path)

    cells_props = pd.DataFrame({})
    embryo_props = pd.DataFrame({})
    for tp in tqdm.tqdm(range(int(meta["n_tp"])), total=int(meta["n_tp"])):

        fname = os.path.join(path, "ch-%d_tp-%03d.tif"%(0, tp))
        img_ch0, ax_names, ax_scales, ax_units = open_ij_tiff(fname)

        fname = os.path.join(path, "ch-%d_tp-%03d.tif"%(1, tp))
        if os.path.exists(fname):
            img_ch1, ax_names, ax_scales, ax_units = open_ij_tiff(fname)
        else:
            img_ch1 = np.zeros(img_ch0.shape)

        if not use_gpu:
            fname = os.path.join(path, "ch-%d_tp-%03d_labels.tif"%(0, tp))
        else:
            fname = os.path.join(path, "ch-%d_tp-%03d_labels_gpu.tif"%(0, tp))
        labels, ax_names, ax_scales, ax_units = open_ij_tiff(fname)

        ### extract information on cells
        def surface_area(regionmask):
            try:
                v, f, _, _ = marching_cubes(regionmask, 
                                            spacing=ax_scales
                                            )
                a = mesh_surface_area(v, f)
            except:
                a = -1
            return a
        
        def ellipsoid_fit(regionmask):
            '''
            from
            https://github.com/aleksandrbazhin/ellipsoid_fit_python/blob/master/ellipsoid_fit.py
            '''
            try:
                edge = regionmask ^ binary_erosion(regionmask)
                # print(edge.shape)
                X = np.asarray(np.where(edge)).transpose()
                hull = ConvexHull(X)
                X_hull = X[hull.vertices]
                # print(hull)
                # print(X.shape)
                x = X_hull[:, 0]
                y = X_hull[:, 1]
                z = X_hull[:, 2]
                D = np.array([x * x + y * y - 2 * z * z,
                            x * x + z * z - 2 * y * y,
                            2 * x * y,
                            2 * x * z,
                            2 * y * z,
                            2 * x,
                            2 * y,
                            2 * z,
                            1 - 0 * x])
                d2 = np.array(x * x + y * y + z * z).T # rhs for LLSQ
                u = np.linalg.solve(D.dot(D.T), D.dot(d2))
                a = np.array([u[0] + 1 * u[1] - 1])
                b = np.array([u[0] - 2 * u[1] - 1])
                c = np.array([u[1] - 2 * u[0] - 1])
                v = np.concatenate([a, b, c, u[2:]], axis=0).flatten()
                A = np.array([[v[0], v[3], v[4], v[6]],
                            [v[3], v[1], v[5], v[7]],
                            [v[4], v[5], v[2], v[8]],
                            [v[6], v[7], v[8], v[9]]])

                center = np.linalg.solve(- A[:3, :3], v[6:9])

                translation_matrix = np.eye(4)
                translation_matrix[3, :3] = center.T

                R = translation_matrix.dot(A).dot(translation_matrix.T)

                evals, evecs = np.linalg.eig(R[:3, :3] / -R[3, 3])
                evecs = evecs.T

                radii = np.sqrt(1. / np.abs(evals))
                radii *= np.sign(evals)
                # sometimes the fit is wrong and you get negative values...
                radii = np.clip(radii, 0, None)
                # order axis from larger to smaller
                radii = np.sort(radii)[::-1]
            except:
                # sometimes the fit is wrong and you get an error, deal with it by setting all axis at 1
                return 1,1,1

            return radii
        
        props = regionprops_table(
                        label_image=labels,
                        intensity_image=np.moveaxis( np.asarray([img_ch0, img_ch1]), 0, -1 ),
                        spacing=ax_scales,
                        properties=("label","bbox",
                                    "area",
                                    "centroid",
                                    "euler_number",
                                    "extent",
                                    "intensity_mean",
                                    "intensity_max",
                                    "intensity_min",
                                    ),
                        separator="_"
                        )
        
        # compute extra properties
        surface = np.asarray([0. for i in range(len(props["label"]))])
        # print(len(props),len(props["label"]))
        ax_ellipsoid = np.asarray([[0.,0.,0.] for i in range(len(props["label"]))])
        for index in tqdm.tqdm(range(len(props["label"])), total=len(props["label"])):
            # print(index, props["label"][index])
            bbox = [props["bbox_0"][index],props["bbox_1"][index],props["bbox_2"][index],
                    props["bbox_3"][index],props["bbox_4"][index],props["bbox_5"][index]]
            regionmask = labels[bbox[0]:bbox[3],bbox[1]:bbox[4],bbox[2]:bbox[5]]
            regionmask = regionmask==props["label"][index]
            # print(regionmask.shape)
            surface[index] = surface_area(regionmask)
            ax_ellipsoid[index] = ellipsoid_fit(regionmask)

        df = pd.DataFrame(props)

        # print(df)

        df["volume"] = df["area"]
        df["surface"] = surface.astype(int)
        df["major_axis_ellipsoid"] = ax_ellipsoid[:,0] * ax_scales[0]
        df["medium_axis_ellipsoid"] = ax_ellipsoid[:,1] * ax_scales[0]
        df["minor_axis_ellipsoid"] = ax_ellipsoid[:,2] * ax_scales[0]

        df = df.drop(["area", 
                      ], axis=1)

        df.centroid_0 = df.centroid_0.astype(int)
        df.centroid_1 = df.centroid_1.astype(int)
        df.centroid_2 = df.centroid_2.astype(int)

        df["elongation_ellipsoid"] = 1-df.medium_axis_ellipsoid/df.major_axis_ellipsoid
        df["flatness_ellipsoid"] = 1-df.minor_axis_ellipsoid/df.medium_axis_ellipsoid

        df["surface_over_volume"] = df.surface/df.volume

        # find cell distance from sphere surface
        x = df["centroid_0"]
        y = df["centroid_1"]
        z = df["centroid_2"]
        r, cx, cy, cz = fit_sphere(x, y, z)
        pos = np.array([x,y,z]).transpose()
        center = np.array([cx,cy,cz]).transpose()
        df["distance_from_embryo_center"] = np.array([ np.linalg.norm(p-center) for p in pos ])
        df["distance_from_embryo_surface"] = np.array([ np.abs(np.linalg.norm(p-center)-r) for p in pos ])
        
        # find distances to 10 closest neighbors
        nbrs = NearestNeighbors(n_neighbors=11, algorithm='ball_tree').fit(pos)
        distances, _ = nbrs.kneighbors(pos)
        for i in range(10):
            df["distance_to_neighor_%02d"%(i+1)] = distances[:,i+1]
        avg_distances = np.mean(distances[:,1:], 1)
        df["average_distance_to_10neighbors"] = avg_distances
        
        # find Delaunay triangulation and number of connections
        tri = Delaunay(pos)
        n_connections = [0 for i in range(pos.shape[0])]
        for p in range(pos.shape[0]):
            n_connections[p] = np.sum(tri.simplices==p)
        df["delaunay_connections"] = n_connections

        df["tp"] = tp

        ### Compute whole embryo statistics
        df1 = pd.DataFrame({
            "tp": [tp],
            "embryo_center_0": cx,
            "embryo_center_1": cy,
            "embryo_center_2": cz,
            "embryo_radius": [r],  
            "n_cells": pos.shape[0],
        })

        # print(df1)

        cells_props = pd.concat([cells_props, df], axis=0, ignore_index=True)
        embryo_props = pd.concat([embryo_props, df1], axis=0, ignore_index=True)
        
    cells_props = compute_relative_distances(cells_props, embryo_props)

    if not use_gpu:
        file_name1 = os.path.join(path, "cells_props.csv")
        file_name2 = os.path.join(path, "embryo_props.csv")
    else:
        file_name1 = os.path.join(path, "cells_props_gpu.csv")
        file_name2 = os.path.join(path, "embryo_props_gpu.csv")

    cells_props.to_csv(file_name1)
    embryo_props.to_csv(file_name2)

    return df, df1
