import pandas as pd

def clean_props_df(df):
    if "frame" not in df.keys():
        df["frame"] = df.tp
    for key in df.keys():
        if "Unnamed" in key:
            df = df.drop(key, axis=1)
            
    return df