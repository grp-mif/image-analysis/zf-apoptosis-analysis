import numpy as np
import math

def fit_sphere(x,y,z):
    #   Assemble the A matrix
    spX = np.array(x)
    spY = np.array(y)
    spZ = np.array(z)
    A = np.zeros((len(spX),4))
    A[:,0] = spX*2
    A[:,1] = spY*2
    A[:,2] = spZ*2
    A[:,3] = 1

    #   Assemble the f matrix
    f = np.zeros((len(spX),1))
    f[:,0] = (spX*spX) + (spY*spY) + (spZ*spZ)
    C, residules, rank, singval = np.linalg.lstsq(A,f,rcond=None)

    #   solve for the radius
    t = (C[0]*C[0])+(C[1]*C[1])+(C[2]*C[2])+C[3]
    radius = math.sqrt(t)

    return radius, C[0], C[1], C[2]

if __name__=="__main__":
    import os
    import pandas as pd
    from _read_meta_csv import read_meta_csv
    import matplotlib.pyplot as plt
    
    path = "W:\\Users\\Laura_Bianchi\\MuVi_processed\\2024-02-15_sample0_control"
    path = "/mif-users/Users/Laura_Bianchi/MuVi_processed/2024-02-15_sample0_control"
    cellprops_file = os.path.join(path, "cells_props.csv")
    
    meta = read_meta_csv(path)
    df = pd.read_csv(cellprops_file)
    
    tps=30
    
    from tqdm import tqdm
    
    for tp in tqdm(range(tps), total=tps):
    
        x = df[df.tp==tp]["centroid_0"]
        y = df[df.tp==tp]["centroid_1"]
        z = df[df.tp==tp]["centroid_2"]
        
        fig1 = plt.figure(figsize=(12,12))
        ax = fig1.add_subplot(projection='3d')
        ax.scatter(x,y,z, c='k', alpha=0.5)
        ax.view_init(elev=-80,azim=-62,roll=0)
        ax.set_xlim(200,800)
        ax.set_ylim(200,800)
        ax.set_zlim(200,800)
        ax.set_box_aspect([1,1,1])
        
        r, cx, cy, cz = fit_sphere(x,y,z)
        
        ax.scatter(cx,cy,cz, c="r", alpha=1)
        
        u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
        xs = r*np.cos(u)*np.sin(v)
        ys = r*np.sin(u)*np.sin(v)
        zs = r*np.cos(v)
        ax.plot_wireframe(xs+cx, ys+cy, zs+cz, color="g")
        
        pos = np.array([x,y,z]).transpose()
        center = np.array([cx,cy,cz]).transpose()
        print(pos.shape)
        
        distances = np.array([ np.abs(np.linalg.norm(p-center)-r) for p in pos ])
        fig2, ax = plt.subplots(1,1)
        ax.hist(distances, bins=100)
        ax.set_yscale('log')
        ax.set_xlabel("Distance from sphere surface (um)")
        ax.set_ylabel("Number of cells (#)")
        
        # from sklearn.neighbors import LocalOutlierFactor
        # clf = LocalOutlierFactor(n_neighbors=20, contamination=0.1)
        # y_pred = clf.fit_predict(distances)
        
        # plt.show()
        
        fig1.savefig(os.path.join("..","..","..","sphere_fit","vis_tp%02d.png"%tp))
        fig2.savefig(os.path.join("..","..","..","sphere_fit","dist_tp%02d.png"%tp))
        
        plt.close(fig1)
        plt.close(fig2)
        
        