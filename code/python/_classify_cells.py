from skimage.io import imread
from pyclesperanto_prototype import imshow, replace_intensities
from skimage.measure import label, regionprops
import os, glob, re, tqdm
import numpy as np
import pandas as pd
from _read_meta_csv import read_meta_csv
from _compute_relative_distances import compute_relative_distances
from sklearn.ensemble import RandomForestClassifier
# from sklearn.neural_network import MLPClassifier

def train_classifier(path, tps_annotated, points_df):

    features = ["tp","extent","intensity_mean_0","intensity_min_0","intensity_max_0",
                    "volume","surface","major_axis_ellipsoid","medium_axis_ellipsoid","minor_axis_ellipsoid",
                    "elongation_ellipsoid","flatness_ellipsoid","surface_over_volume",
                    "rel_distance_center", "rel_distance_surface",
                    "average_distance_to_10neighbors","delaunay_connections"]
    training_data = np.zeros((1,len(features)))
    ground_truth = []
    for tp in tqdm.tqdm(tps_annotated, total=len(tps_annotated)):
        # print(tp)
        
        # load data for this tmepoint
        labels = imread(os.path.join(path,'ch-0_tp-%03d_labels.tif'%tp))
        annotation = imread(os.path.join(path,'ch-0_tp-%03d_annotations.tif'%tp))
        statistics = points_df[points_df.frame==tp]

        # extract features to use for the classifier
        table = statistics[features]#+["distance_to_neighor_%02d"%(i+1) for i in range(10)]]

        # find out ground truth data for annotated cells
        annotation_stats = regionprops(labels, intensity_image=annotation)
        annotated_classes = np.asarray([s.max_intensity for s in annotation_stats])
        ground_truth_tp = np.asarray(annotated_classes[annotated_classes>0])
        # print("1",ground_truth_tp.shape)

        # find out training set of ground truth cells
        training_data_tp = table.iloc[list(np.where(annotated_classes>0)[0])].to_numpy()
        # print("2",training_data_tp.shape)
        # print("3",np.array(training_data).shape)
        
        training_data = np.concatenate((training_data, training_data_tp), axis=0)
        # print("4",np.array(training_data).shape)
        ground_truth = np.asarray(list(ground_truth)+list(ground_truth_tp))
        
    training_data = training_data[1:]

    # train the classifier
    classifier = RandomForestClassifier(max_depth=5, n_estimators=10, random_state=0)
    classifier.fit(training_data, ground_truth)

    return classifier

def predict_classes(statistics, classifier):
    
    # extract cell features for this timepoint
    table = statistics[["tp","extent","intensity_mean_0","intensity_min_0","intensity_max_0",
                "volume","surface","major_axis_ellipsoid","medium_axis_ellipsoid","minor_axis_ellipsoid",
                "elongation_ellipsoid","flatness_ellipsoid","surface_over_volume",
                "rel_distance_center", "rel_distance_surface",
                "average_distance_to_10neighbors","delaunay_connections"]]#+["distance_to_neighor_%02d"%(i+1) for i in range(10)]]
    
    classes = list(classifier.predict(table.to_numpy()))
    
    return classes

def classify_cells(path, tps=None, save=False):
    
    # load metadata
    meta = read_meta_csv(path)
    if tps is None:
        tps = range(int(meta["n_tp"]))
    
    # load annotated timepoint list
    annotation_list = glob.glob(os.path.join(path, "ch-0_tp-*_annotations.tif"))
    tps_annotated = [int(re.findall(r"tp-(\d{3})_annotations", s)[0]) for s in annotation_list]
    tps_annotated.sort()
    
    # load cell properties
    print("Load csv files..")
    points_df = pd.read_csv(os.path.join(path, "cells_props.csv"))
    points_df["frame"] = points_df.tp
    # load embryo properties    
    embryo_df = pd.read_csv(os.path.join(path, "embryo_props.csv"))
    embryo_df["frame"] = embryo_df.tp

    # compute distances as relative to radius
    print("Compute relative distances...")
    if "rel_distance_center" not in points_df.keys():
        points_df = compute_relative_distances(points_df, embryo_df)
        points_df.to_csv(os.path.join(path, "cells_props.csv"))

    # compute random forest classifiers for all timepoints available
    print("Training classifiers...")
    classifier = train_classifier(path, tps_annotated, points_df)

    # predict cell classes in all timepoints using the closest (in time) available classifier
    print("Making new predictions...")
    cell_classes = []
    n_evl = [0 for n in tps]
    idx = 0
    for tp in tqdm.tqdm(tps, total = len(tps)):
        statistics = points_df[points_df.frame==tp]
                
        cell_classes_tp = predict_classes(statistics, classifier)
        cell_classes += cell_classes_tp

        n_evl[idx] = sum([c==2 for c in cell_classes_tp])
        idx += 1
        
    points_df["to_process"] = [ tp in tps for tp in points_df.tp ]
    points_df = points_df[points_df["to_process"]] 
    embryo_df["to_process"] = [ tp in tps for tp in embryo_df.tp ]
    embryo_df = embryo_df[embryo_df["to_process"]]
    
    embryo_df["n_EVL_cells"] = n_evl
    points_df["class"] = cell_classes

    if save:
        print("Saving data...")
        file_name = os.path.join(path, "cells_props_classified.csv")
        points_df.to_csv(file_name)
        file_name = os.path.join(path, "embryo_props_classified.csv")
        embryo_df.to_csv(file_name)
    
    print("Done.")
        
    return




