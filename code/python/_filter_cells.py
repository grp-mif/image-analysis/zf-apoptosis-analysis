from skimage.measure import label
from skimage.measure import regionprops_table
import numpy as np
import pandas as pd
from tqdm import tqdm
from _fit_sphere import fit_sphere
from skimage.util import map_array
from sklearn.neighbors import NearestNeighbors

def filter_cells(labels, ax_scale, max_distance_from_surface, max_distance_to_10neighbors):
    # First filter: compute euler_number
    print("\tFind euler number...")
    props = regionprops_table(
                    label_image=labels,
                    spacing=ax_scale,
                    properties=("label",
                                "euler_number",
                                "centroid",
                                "extent",
                                ),
                    )  
    df = pd.DataFrame(props)

    # Second filter: compute distance from sphere surface
    print("\tFind distance from sphere surface...")
    x = df["centroid-0"]
    y = df["centroid-1"]
    z = df["centroid-2"]

    r, cx, cy, cz = fit_sphere(x, y, z)
    pos = np.array([x,y,z]).transpose()
    center = np.array([cx,cy,cz]).transpose()
    df["distance_from_embryo_surface"] = np.array([ np.abs(np.linalg.norm(p-center)-r) for p in pos ])

    # Third filter: compute average distance to 10 neighbors
    print("\tFind average distance to 10 closest neighbors...")
    nbrs = NearestNeighbors(n_neighbors=11, algorithm='ball_tree').fit(pos)
    distances, _ = nbrs.kneighbors(pos)
    avg_distances = np.mean(distances[:,1:], 1)
    df["distance_to_10neighbors"] = avg_distances

    print("\tFilter cells...")
    props_filt = df[(df.euler_number!=1)|
                    (df.distance_from_embryo_surface>max_distance_from_surface)|
                    (df.distance_to_10neighbors>max_distance_to_10neighbors)
                    ]#|(df.extent<0.2)]

    labels_to_filter = np.asarray(props_filt.label)
    new_labels = list(df.label)
    for i, l in enumerate(list(df.label)):
        if l in labels_to_filter:
            new_labels[i] = 0
    
    labels = map_array(labels, np.array(list(df.label)), np.array(new_labels))
    new_labels = label(labels)
    print("\tNumber of cells after filtering:", new_labels.max())

    return new_labels