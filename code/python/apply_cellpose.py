import numpy as np
import matplotlib.pyplot as plt
from cellpose import core, models

'''
Adapted from:
https://github.com/pr4deepr/cellpose-colab/blob/main/Cellpose_cell_segmentation_2D_prediction_only.ipynb
'''

def define_model(model_name):

    #@markdown diameter of cells (set to zero to use diameter from training set):
    # diameter =  0#@param {type:"number"}
    #@markdown threshold on flow error to accept a mask (set higher to get more cells, e.g. in range from (0.1, 3.0), OR set to 0.0 to turn off so no cells discarded):
    flow_threshold = 0. #@param {type:"slider", min:0.0, max:3.0, step:0.1}
    #@markdown threshold on cellprob output to seed cell masks (set lower to include more pixels or higher to include fewer, e.g. in range from (-6, 6)):
    cellprob_threshold=0 #@param {type:"slider", min:-6, max:6, step:1}

    model = models.CellposeModel(
                        gpu=True, 
                        model_type=model_name,
                        diam_mean=8
                        )

    # use model diameter if user diameter is 0
    diameter = model.diam_labels# if diameter==0 else diameter

    # run model on test images
    
    return model, diameter, flow_threshold, cellprob_threshold

def predict_volume(image, meta, 
                   model, diameter, flow_threshold, cellprob_threshold):
    
    anisotropy = meta["dataset_scale_z"]/meta["dataset_scale_x"]

    mask, _, _ = model.eval(
                                    image, 
                                    channels= [0, 0],
                                    diameter=diameter,
                                    flow_threshold=flow_threshold,
                                    cellprob_threshold=cellprob_threshold,
                                    do_3D=True,
                                    anisotropy=anisotropy,
                                    tile=True
                                    )
    
    return mask

if __name__ == "__main__":
    use_GPU = core.use_gpu()
    yn = ['NO', 'YES']
    print(f'>>> GPU activated? {yn[use_GPU]}')
