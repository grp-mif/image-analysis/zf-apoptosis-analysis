import os, glob, tqdm, csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from _read_meta_csv import read_meta_csv

if __name__=="__main__":
    '''
    To run this script:

    >>> python run_cell_analysis.py --share /mif-users --radius 1,1,1 --min_distance 1
    '''

    def list_of_floats(arg):
        l = [float(f) for f in arg.split(",")]
        # print("---",l)
        return l

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--share", 
                        type=str, default="/mif-users", 
                        help="Server share. /mif-users (Linux) or //mif-users.embl.es/mif-users (Windows)")
    parser.add_argument("--master_folder",
                        type=str, default="Users/Laura_Bianchi")
    parser.add_argument("--exp_folder",
                       type=str, default="MuVi_processed")
    
    # parser.add_argument("--radius_gauss", 
    #                     type=list_of_floats, default=[1., 1., 1.],
    #                     help="Radius for gaussian blur in um.")
    # parser.add_argument("--min_distance", 
    #                     type=int, default=1,
    #                     help="Min distance between peaks in pixels.")
    # parser.add_argument("--min_peak_val", 
    #                     type=int, default=20,
    #                     help="Min intensity values for peaks.")

    # parser.add_argument("--nucleus_radius_lims", 
    #                     type=list_of_floats, default=[2., 25.],
    #                     help="Min and max nucleus radius.")
    # parser.add_argument("--threshold_watershed", 
    #                     type=int, default=5,
    #                     help="Watershed threshold.")

    args = parser.parse_args()

    share = args.share
    master_folder = args.master_folder
    exp_folder = args.exp_folder

    # radius_gauss = args.radius_gauss
    # min_distance = args.min_distance
    # min_peak_val = args.min_peak_val
    
    # nucleus_radius_lims = args.nucleus_radius_lims
    # threshold_watershed = args.threshold_watershed

    # min_nucleus_radius = nucleus_radius_lims[0]
    # max_nucleus_radius = nucleus_radius_lims[1]

    #--------------------------------------------------------

    paths = glob.glob(os.path.join(share, master_folder, exp_folder, "*"))
    paths.sort()
    paths = [path for path in paths if "_BAD" not in path]
    paths = [path for path in paths if "test" not in path]
    # paths = [path for path in paths if "2023" in path]
    print(paths)
    # paths = [paths[0]]

    n_exps = len(paths)
    n_controls = 0
    n_bax = 0
    for path in paths:
        cellprops_file = os.path.join(path, "cells_props.csv")
        if os.path.exists(cellprops_file):
            meta = read_meta_csv(path)
        if meta["condition"] == "control":
            n_controls += 1
        else:
            n_bax += 1

    # print(n_controls, n_bax)
    fig, ax = plt.subplots(1,1)
    fig1, ax1 = plt.subplots(n_exps,3)
    fig2, ax2 = plt.subplots(n_exps,3)

    colors = {"control":"gray", "BAX":"blue"}
    t_offsets = [0.5,0.]

    k = 0
    for path in paths:
        cellprops_file = os.path.join(path, "cells_props.csv")
        if os.path.exists(cellprops_file):
            meta = read_meta_csv(path)
            if meta["date"]=="2023-11-20":
                t_offset = t_offsets[0]
            else:
                t_offset = t_offsets[1]

            df = pd.read_csv(cellprops_file)
            # print(df.keys())

            n_cells = [len(df[df.tp==tp]) for tp in range(int(meta["n_tp"]))]
            ax.plot(np.arange(len(n_cells))*15/60+t_offset, n_cells,"-o", label=meta["sample"]+"_"+meta["condition"],
                    color=colors[meta["condition"]], alpha=1.)
            
            ### plot histogram of BAX signal intensity
            df_0 = df[df.tp==0]
            ax1[k,0].hist(df_0.intensity_mean_1,bins=100,range=(0,500), color=colors[meta["condition"]])
            df_1 = df[df.tp==(np.max(df.tp)//2)]
            ax1[k,1].hist(df_1.intensity_mean_1,bins=100,range=(0,5000), color=colors[meta["condition"]])
            df_2 = df[df.tp==np.max(df.tp)]
            ax1[k,2].hist(df_2.intensity_mean_1,bins=100,range=(0,5000), color=colors[meta["condition"]])

            ax1[k,0].set_yscale("log")
            ax1[k,1].set_yscale("log")
            ax1[k,2].set_yscale("log")

            ### plot histogram of volume
            df_0 = df[df.tp==0]
            ax2[k,0].hist(df_0.volume,bins=10,range=(0,200), color=colors[meta["condition"]])
            df_1 = df[df.tp==(np.max(df.tp)//2)]
            ax2[k,1].hist(df_1.volume,bins=10,range=(0,200), color=colors[meta["condition"]])
            df_2 = df[df.tp==np.max(df.tp)]
            ax2[k,2].hist(df_2.volume,bins=10,range=(0,200), color=colors[meta["condition"]])

            ax2[k,0].set_ylim(0,100)
            ax2[k,1].set_ylim(0,100)
            ax2[k,2].set_ylim(0,100)

            # ax2[k,0].set_yscale("log")
            # ax2[k,1].set_yscale("log")
            # ax2[k,2].set_yscale("log")

            k += 1


    # ax.legend()
    ax.set_xlabel("Time (hours)")
    ax.set_ylabel("Number of cells")
    plt.show()

