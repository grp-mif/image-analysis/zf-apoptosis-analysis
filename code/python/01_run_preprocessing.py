import os
import glob
import argparse
from _preprocess import preprocess

'''
To run this script:

>>> python run_preprocessing.py --share /mif-users --radius_enhanced 1,1,1
'''

def list_of_floats(arg):
    l = [float(f) for f in arg.split(",")]
    # print("---",l)
    return l

parser = argparse.ArgumentParser()
parser.add_argument("--share", 
                    type=str, default="/mif-users", 
                    help="Server share. /mif-users (Linux) or //mif-users.embl.es/mif-users (Windows)")
parser.add_argument("--master_folder",
                    type=str, default="Users/Laura_Bianchi")
parser.add_argument("--exp_folder",
                    type=str, default="2024-02-15_162639/processed")

parser.add_argument("--desired_scale", 
                    type=list_of_floats, default=1.,
                    help="Desired pixel size in um after downsampling (provide one value for isotropic resolution).")
parser.add_argument("--radius_enhanced", 
                    type=list_of_floats, default=1.,
                    help="Desired radius enhancement in um (provide one value for isotropic ZYX dimension).")

args = parser.parse_args()

share = args.share
master_folder = args.master_folder
exp_folder = args.exp_folder
desired_scale = args.desired_scale
radius_enhanced = args.radius_enhanced

#--------------------------------------------------------

paths = glob.glob(os.path.join(share, master_folder, exp_folder, "*"))
paths.sort()
# paths = paths[-2:]
# paths = [path for path in paths if path.endswith("_BAX_C")]
print(paths)

for path in [paths[-1]]:
    print(10*"-",path)
    preprocess(path, share, master_folder, desired_scale)
