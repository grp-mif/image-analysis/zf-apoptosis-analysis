# from skimage.io import imread
import os, platform
import dask.array as da
from dask_image.imread import imread
import napari
# import numpy as np
import pandas as pd
# import pyclesperanto_prototype as cle
from tqdm import tqdm
from _read_meta_csv import read_meta_csv
from napari_skimage_regionprops._table import add_table

'''
adapted from:

https://napari.org/stable/tutorials/processing/dask.html
'''

if platform.system() == "Windows":
    folder = "//mif-users.embl.es/mif-users/Users/Laura_Bianchi/MuVi_processed/"
else:
    folder = "/mif-users/Users/Laura_Bianchi/MuVi_processed/"
# exp = "2023-11-20_sample0_BAX"
# exp = "2024-02-15_sample5_BAX"
# exp = "2024-02-15_sample0_control"
exp = "test_data_new"

###################################################################
meta = read_meta_csv(os.path.join(folder, exp))
n_channels = int(meta["n_ch"])
n_tps = int(meta["n_tp"])

### define file name pattern
file_pattern = os.path.join(folder, exp, "ch-{:d}_tp-{:03d}.tif")
# file_pattern_enh = os.path.join(folder, exp, "ch-0_tp-{:03d}_enhanced.tif")
file_pattern_lab = os.path.join(folder, exp, "ch-0_tp-{:03d}_labels.tif")

### read files with dask image
dataset = {}
for ch in range(n_channels):
    dataset["ch%d"%ch] = [imread(file_pattern.format(ch,j)) for j in range(n_tps)]
# dataset_enh = [imread(file_pattern_enh.format(j)) for j in range(n_tps)]
dataset_lab = [imread(file_pattern_lab.format(j)) for j in range(n_tps)]

stack = {}
for ch in range(n_channels):
    stack["ch%d"%ch] = da.stack(dataset["ch%d"%ch])#.compute()
# stack_enh = da.stack(dataset_enh).compute()
stack_lab = da.stack(dataset_lab)#.compute()

### setup napari viewer
v = napari.Viewer()

colormaps = ["cyan", "magenta"]
names = ["nuclei", "BAX"]
lims = [[0,5000], [0,10000]]
for ch in range(n_channels):
    v.add_image(stack["ch%d"%ch], contrast_limits=lims[ch], multiscale=False, 
                # channel_axis=0,
                colormap=colormaps[ch], 
                blending="additive",
                name=names[ch]
                )

# v.add_image(stack_enh, contrast_limits=[0,5000], multiscale=False, 
#             colormap="cyan",
#             name="nuclei_enh"
#             )

label_layer = v.add_labels(stack_lab, 
            name="nuclei_lab",
            opacity=0.2
            )


# Add properties table to napari viewer
points_df = pd.read_csv(os.path.join(folder, exp, "cells_props_classified.csv"))
points_df["frame"] = points_df.tp
# points_df = points_df[points_df["frame"]<3]

# taken from https://forum.image.sc/t/exloring-image-segment-features-in-napari/75222/6
label_layer.features = points_df
add_table(label_layer, v)

napari.run()




