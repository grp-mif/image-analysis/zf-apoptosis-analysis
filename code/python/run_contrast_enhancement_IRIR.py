#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 15:25:17 2018

@author: ngritti
"""

from csbdeep.models import CARE
import time, os, glob
# from keras import backend as K
# import tensorflow as tf
import numpy as np
from skimage.io import imread, imsave

# time.sleep(60*60*6)

#%%
'''
register images and save them in the Registered_SIFT subfolder
generate patches if you want to use this DataSet as training for a model
'''
infolder = os.path.join('/g/people/gritti/IRIR')

pathsData = [
    os.path.join("/mif-users/Users/Laura_Bianchi/MuVi_processed/2023-11-20_sample0_BAX"),
    ]
pathModel = os.path.join("/g/mif/people/gritti/IRIR/pescoids/stained/p2/model")

for pathData in pathsData:
    
    
    start = time.time()
    print('\n**********')
    print('*** Data: ',pathData,'***')
    print('*** Model: ',pathModel,'***')
    
    modelName = "model_pescoid"

    if not os.path.exists(os.path.join(pathData,'restored_with_'+modelName)):
        os.mkdir(os.path.join(pathData,'restored_with_'+modelName))

    if not os.path.exists(os.path.join(pathData,'restored_with_'+modelName,'restored.tif')):
        
        # load ground truth
        print('Loading input...') 
        flist = glob.glob(os.path.join(pathData,'ch-0_tp-*.tif'))
        flist = [f for f in flist if "enhanced" not in f]
        flist.sort()

        for f in flist:
            _input = imread(f)
            _input = np.expand_dims(_input,0)
            print(_input.shape)
            print(os.path.join(*pathModel.split(os.sep)[:-1]))
            model = CARE(config=None, name=modelName, 
                            basedir=pathModel)
    
            r = model.predict(_input, axes='CZYX', n_tiles=(1,2,4,4))
            #restored = (restored-np.min(restored))/(np.max(restored)-np.min(restored))
            #restored = restored*(lims[1][1]-lims[1][0])+lims[1][0]
            #restored = restored.astype(np.uint16)
            
            # # rescale restored image to minimize mse
            # N = np.product(y.shape)
            # alpha = (np.sum(y/N*r/N)-np.sum(y/N)*np.sum(r/N)/N)/(np.sum((r/N)**2)-np.sum(r/N)**2/N)
            # beta = np.sum(y-alpha*r)/N
            # r = alpha*r+beta
            r = np.clip(r,0,2**16-1)
            r = r.astype(np.uint16)
        
            imsave(os.path.join(pathData,'restored_with_'+modelName,f.split(os.sep)[-1]),r)