import numpy as np
import math

import os
import pandas as pd
from _read_meta_csv import read_meta_csv
import matplotlib.pyplot as plt

path = "W:\\Users\\Laura_Bianchi\\MuVi_processed\\2024-02-15_sample0_control"
path = "/mif-users/Users/Laura_Bianchi/MuVi_processed/2024-02-15_sample0_control"
path = "/mif-users/Users/Laura_Bianchi/MuVi_processed/2023-11-20_sample0_BAX"
cellprops_file = os.path.join(path, "cells_props.xlsx")

meta = read_meta_csv(path)
df = pd.read_excel(cellprops_file, sheet_name="cell_properties")
df1 = pd.read_excel(cellprops_file, sheet_name="embryo_properties")

tps=8

from tqdm import tqdm

for tp in tqdm(range(tps), total=tps):

    x = df[df.tp==tp].centroid_0
    y = df[df.tp==tp].centroid_1
    z = df[df.tp==tp].centroid_2

    fig1 = plt.figure(figsize=(12,12))
    ax = fig1.add_subplot(projection='3d')
    ax.scatter(x,y,z, c='k', alpha=0.5)
    ax.view_init(elev=-80,azim=-62,roll=0)
    ax.set_xlim(200,800)
    ax.set_ylim(200,800)
    ax.set_zlim(200,800)
    ax.set_box_aspect([1,1,1])
    
    cx = df1[df1.tp==tp].embryo_center_0.values[0]
    cy = df1[df1.tp==tp].embryo_center_1.values[0]
    cz = df1[df1.tp==tp].embryo_center_2.values[0]

    r = df1[df1.tp==tp].embryo_radius.values[0]
    
    ax.scatter(cx,cy,cz, c="r", alpha=1)
    
    u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
    xs = r*np.cos(u)*np.sin(v)
    ys = r*np.sin(u)*np.sin(v)
    zs = r*np.cos(v)
    ax.plot_wireframe(xs+cx, ys+cy, zs+cz, color="g")
    
    pos = np.array([x,y,z]).transpose()
    center = np.array([cx,cy,cz]).transpose()
    print(pos.shape)
    
    distances = df[df.tp==tp].distance_from_embryo_surface
    fig2, ax = plt.subplots(1,1)
    ax.hist(distances, bins=100)
    ax.set_yscale('log')
    ax.set_xlabel("Distance from sphere surface (um)")
    ax.set_ylabel("Number of cells (#)")
    
    # from sklearn.neighbors import LocalOutlierFactor
    # clf = LocalOutlierFactor(n_neighbors=20, contamination=0.1)
    # y_pred = clf.fit_predict(distances)
    
    # plt.show()
    
    fig1.savefig(os.path.join("..","..","..","sphere_fit_1","vis_tp%02d.png"%tp))
    fig2.savefig(os.path.join("..","..","..","sphere_fit_1","dist_tp%02d.png"%tp))
    
    plt.close(fig1)
    plt.close(fig2)
    
    