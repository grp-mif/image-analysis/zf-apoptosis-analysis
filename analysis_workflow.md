# Analysis workflow

Description of the analysis workflow, including instructions for visual QC of the analysis results.

### Installation

For CPU installation, TODO

For GPU usage, install `devbio-napari` plugins from [here](https://github.com/haesleinhuepf/devbio-napari) and `dask-image` from [here](https://image.dask.org/en/latest/) or [here](https://www.napari-hub.org/plugins/devbio-napari). Additionally, install `h5py` and `openpyxl`, which is not included in the `devbio-napari` package.
